# Ledstrip Controller for Arduino version 1

This program created in Java was a first attempt to creating software to control ledstrips.

# Functionality

*  Connect an Arduino to the PC.
*  The software collects information of the serial ports and will show the Arduino in a list menu.
*  Select the Arduino type in the list.
*  Select the COM port.
*  Select an effect from either static or dynamic, these are all premade.
*  Click "Upload".
*  When you disconnect the arduino from the serial port, the program will update the list automatically.

# Screenshots

![screenshot1](/uploads/3bfa40eea6cee3c7a696d6b54aca27e5/screenshot1.PNG)

![screenshot2](/uploads/ee711d0f317e001417c0333988f6ec05/screenshot2.PNG)

# Unfinished

The "Games" tab was an idea to attach a certain effect when a game is started, but it was cancelled in the process.