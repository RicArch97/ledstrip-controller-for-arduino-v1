package Ledstripcontroller;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class MyPanel extends JPanel{
	BufferedImage img;
	
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(img, 0, 0, getWidth(), getHeight(), this);
    }
	
	public void setImage(BufferedImage img) {
		this.img = img;
	}
}
