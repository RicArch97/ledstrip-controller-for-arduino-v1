package Ledstripcontroller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class DynamicTab {
	MyPanel Dynamic;
	JTextArea dynamicText;
	ArrayList<Devices> dev;
	ReadCOM com;
	Elements elements;
	JTabbedPane tabbedPane;
	BufferedImage img;
	DeviceConfig config;
	
	public DynamicTab(JTabbedPane tabbedPane, BufferedImage img, final DeviceConfig config, Elements elements, ReadCOM com, ArrayList<Devices> dev) {
		this.dev = dev;
		this.com = com;
		this.elements = elements;
		this.tabbedPane = tabbedPane;
		this.img = img;
		this.config = config;
	}
	
	public void dynamicTab()
	{
		Dynamic = new MyPanel();
		Dynamic.setImage(img);
		dynamicText = new JTextArea();
		tabbedPane.addTab("Dynamic", null, Dynamic, null);
		Dynamic.setLayout(null);
		JLabel lab = new JLabel();
		lab.setText("Dynamic");
		lab.setForeground(Color.WHITE);
		lab.setFont(new Font("Poor Richard", Font.BOLD, 50));
		lab.setPreferredSize(new Dimension(297, 100));
		tabbedPane.setTabComponentAt(2, lab);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(150, 50, 255, 400);
		Dynamic.add(scrollPane);
		
		JList<String> list_2 = new JList<String>();
		list_2.setFont(new Font("Poor Richard", Font.BOLD, 30));
		
		DefaultListModel<String> dlm=new DefaultListModel<String>();
		dlm.addElement("Autumn_Storm");
		dlm.addElement("Bonfire");
		dlm.addElement("Lightning_Struck");
		dlm.addElement("Merry_Christmas");
		dlm.addElement("Nuclear_Hazard");
		dlm.addElement("Rainbow");
		dlm.addElement("Romantic_Red");
		dlm.addElement("Star_Light");
		dlm.addElement("Winter_Wonder");
		dlm.addElement("Yin_Yang");
		list_2.setModel(dlm);
		scrollPane.setViewportView(list_2);
		
		list_2.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent arg0) {
            	JList<?> list = (JList<?>)arg0.getSource();
                if (!arg0.getValueIsAdjusting()) {
                	elements.getDynamic().setValue(0);
                	elements.getDynamic().setString(null);
                	
                	if (list.getSelectedValue().equals("Autumn_Storm"))
        			{
        				config.setHexPath("Hexfiles/Autumn_Storm.ino.hex");
        				dynamicText.setText("Experience an autumn storm of \n beautiful colors, to go through \n autumn with a smile!");
        				dynamicText.setForeground(Color.ORANGE);
        			}
        			
        			if (list.getSelectedValue().equals("Bonfire"))
        			{
        				config.setHexPath("Hexfiles/Bonfire.ino.hex");
        				dynamicText.setText("With the flaming red with bright \n yellow of bonfire, the fire \n lives in your PC and within you.");
        				dynamicText.setForeground(Color.red);
        			}
        			
        			if (list.getSelectedValue().equals("Lightning_Struck"))
        			{
        				config.setHexPath("Hexfiles/Lightning_Struck.ino.hex");
        				dynamicText.setText("Always been scared of lightning, \n but like the epic skyshow of mother \n nature? Turn off the lights \n and experience it without danger!");
        				dynamicText.setForeground(Color.yellow);
        			}
        			
        			if (list.getSelectedValue().equals("Merry_Christmas"))
        			{
        				config.setHexPath("Hexfiles/Merry_Christmas.ino.hex");
        				dynamicText.setText("Merry Christmas! With this effect \n you can make your Christmas even \n brighter!");
        				dynamicText.setForeground(Color.WHITE);
        			}
        			
        			if (list.getSelectedValue().equals("Nuclear_Hazard"))
        			{
        				config.setHexPath("Hexfiles/Nuclear_Hazard.ino.hex");
        				dynamicText.setText("The risks of radioactivity are \n inevitable. With this effect you can \n experience the colors of \n radioactivity without harm.");
        				dynamicText.setForeground(Color.GREEN);
        			}
        			
        			if (list.getSelectedValue().equals("Romantic_Red"))
        			{
        				config.setHexPath("Hexfiles/Romantic_Red.ino.hex");
        				dynamicText.setText("Let this effect share the \n love & romance!");
        				dynamicText.setForeground(Color.RED);
        			}
        			
        			if (list.getSelectedValue().equals("Winter_Wonder"))
        			{
        				config.setHexPath("Hexfiles/Winter_Wonder.ino.hex");
        				dynamicText.setText("What is winter beautiful! \n Let these blue and cool colors \n fresh up your mind!");
        				dynamicText.setForeground(Color.WHITE);
        			}
        			
        			if (list.getSelectedValue().equals("Yin_Yang"))
        			{
        				config.setHexPath("Hexfiles/Yin_Yang.ino.hex");
        				dynamicText.setText("Day or night, man or woman.\n One effect to describe two opposite \n things, but still in balance. One can \n not be there without the other.");
        				dynamicText.setForeground(Color.GRAY);
        			}
        			
        			if (list.getSelectedValue().equals("Rainbow"))
        			{
        				config.setHexPath("Hexfiles/Rainbow.ino.hex");
        				dynamicText.setText("The king of RGB! \n This effect gives you the most \n complete and epic show you will \n ever experience!");
        				dynamicText.setForeground(Color.MAGENTA);
        			}
        			
        			if (list.getSelectedValue().equals("Star_Light"))
        			{
        				config.setHexPath("Hexfiles/Star_Light.ino.hex");
        				dynamicText.setText("A dark sky with stars \n fading in and out� �you wish you \n could always see this without \n going outside� right?");
        				dynamicText.setForeground(Color.BLUE);
        			}
                }
            }
        });
		
		JLabel lblNewLabel = new JLabel("Dynamic Effects");
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setFont(new Font("Viner Hand ITC", Font.PLAIN, 30));
		scrollPane.setColumnHeaderView(lblNewLabel);
		
		JButton btnNewButton_1 = new JButton("UPLOAD");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					RunCommand avr = new RunCommand();
					avr.runCommand(new String[]{"avrdude/bin/avrdude.exe", "-C"+"avrdude/etc/avrdude.conf","-v",config.getDevice(),config.getDeviceVar(),"-P"+config.getPort(),config.getBaudRate(),"-D","-Uflash:w:"+config.getHexPath()+":i"}, config.getFilename());
					
					ReadFile file = new ReadFile();
					file.readFile(config.getFilename(), elements.getStatic(), elements.getDynamic());
				}catch (Exception er) {
					er.printStackTrace();
				}
			}
		});
		
		btnNewButton_1.setFont(new Font("Poor Richard", Font.BOLD, 20));
		btnNewButton_1.setBounds(534, 381, 196, 69);
		Dynamic.add(btnNewButton_1);
		
		elements.getDynamic().setValue(0);
		elements.getDynamic().setStringPainted(true);
		elements.getDynamic().setBounds(412, 503, 718, 35);
		Dynamic.add(elements.getDynamic());
		
		JLabel lblNewLabel_1 = new JLabel("Upload progress:");
		lblNewLabel_1.setForeground(Color.GREEN);
		lblNewLabel_1.setFont(new Font("Poor Richard", Font.BOLD, 40));
		lblNewLabel_1.setBounds(70, 486, 307, 52);
		Dynamic.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Choose COM-port:");
		lblNewLabel_2.setFont(new Font("Poor Richard", Font.BOLD, 20));
		lblNewLabel_2.setForeground(Color.GREEN);
		lblNewLabel_2.setBounds(874, 361, 256, 35);
		Dynamic.add(lblNewLabel_2);
		
		elements.getArduinoDynamic().setBounds(874, 300, 256, 43);
		elements.getArduinoDynamic().addItem("Choose model:");
		elements.getArduinoDynamic().addItem("Arduino Nano");
		elements.getArduinoDynamic().addItem("Arduino Uno");
		elements.getArduinoDynamic().addItem("Arduino Mega 2560");
		Dynamic.add(elements.getArduinoDynamic());
		
		elements.getArduinoDynamic().addItemListener(new ItemListener() {
			
            @Override
            public void itemStateChanged(ItemEvent arg0) {
            	
            	Object item  = elements.getArduinoDynamic().getSelectedItem();
            	
            	if (item == "Arduino Nano") {
                	config.setDevice("-patmega328p");
                	config.setDeviceVar("-carduino");
                	config.setBaudRate("-b57600");
                }
                
                if (item == "Arduino Uno") {
                	config.setDevice("-patmega328p");
                	config.setDeviceVar("-carduino");
                	config.setBaudRate("-b115200");
                }
                
                if (item == "Arduino Mega 2560") {
                	config.setDevice("-patmega2560");
                	config.setDeviceVar("-cwiring");
                	config.setBaudRate("-b115200");
                }
                
                if (elements.getArduinoStatic().getSelectedItem() != elements.getArduinoDynamic().getSelectedItem()) {
                	elements.getArduinoStatic().setSelectedItem(elements.getArduinoDynamic().getSelectedItem());
                }
            }
		});
		
		JLabel lblNewLabel_3 = new JLabel("Choose Arduino model:");
		lblNewLabel_3.setFont(new Font("Poor Richard", Font.BOLD, 20));
		lblNewLabel_3.setForeground(Color.GREEN);
		lblNewLabel_3.setBounds(874, 251, 256, 35);
		Dynamic.add(lblNewLabel_3);
	
		dynamicText.setOpaque(false);
		dynamicText.setEditable(false);
		dynamicText.setFont(new Font("Poor Richard", Font.BOLD, 40));
		dynamicText.setBounds(530, 50, 600, 185);
		Dynamic.add(dynamicText);
		
		elements.getCOMDynamic().setBounds(874, 407, 256, 43);
		Dynamic.add(elements.getCOMDynamic());
		
		elements.getCOMDynamic().addItemListener(new ItemListener() {
			
            @Override
            public void itemStateChanged(ItemEvent arg0) {
            	
            	String SerialPort  = elements.getCOMDynamic().getSelectedItem().toString();
            	
            	for (int i = 0; i < dev.size(); i++) {
            		if (SerialPort.contains(dev.get(i).getComName())) {
            			config.setPort(dev.get(i).getComName());
            		}
            	}
            	
            	if (!(elements.getCOMStatic().getSelectedItem().equals(elements.getCOMDynamic().getSelectedItem()))) {
            		elements.getCOMStatic().setSelectedItem(SerialPort);
            	}
            }   	
		});
	}
}
