package Ledstripcontroller;

public class DeviceConfig {
	private String hexPath; 
	private String port;
	private String device; 
	private String deviceVariable; 
	private String baudRate;
	private String FILENAME = "Save files/avrdudeOutput.txt";
	
	
	public void setHexPath(String hexPath) {
		this.hexPath = hexPath;
	}
	
	public String getHexPath() {
		return hexPath;
	}
	
	public void setPort(String port) {
		this.port = port;
	}
	
	public String getPort() {
		return port;
	}
	
	public void setDevice(String device) {
		this.device = device;
	}
	
	public String getDevice() {
		return device;
	}
	
	public void setDeviceVar(String deviceVariable) {
		this.deviceVariable = deviceVariable;
	}
	
	public String getDeviceVar() {
		return deviceVariable;
	}
	
	public void setBaudRate(String baudRate) {
		this.baudRate = baudRate;
	}
	
	public String getBaudRate() {
		return baudRate;
	}
	
	public String getFilename() {
		return FILENAME;
	}
}
