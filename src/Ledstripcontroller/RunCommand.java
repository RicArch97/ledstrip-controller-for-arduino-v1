package Ledstripcontroller;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class RunCommand {
	
	public void runCommand(String[] cmd, String FILENAME){

	    String s = null;
	    BufferedWriter bw = null;
	    FileWriter fw = null;

	    try {

	      Process p = Runtime.getRuntime().exec(cmd);
	      BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
	      BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
	      
	      fw = new FileWriter(FILENAME);
	      bw = new BufferedWriter(fw);
		  
	      //read the output from the command
	      System.out.println("command out:\n");
	      while ( (s = stdInput.readLine ()) != null) {
	    	  System.out.println(s);
	    	  bw.write(s+"\n");
	      }
	      
	      System.out.println("errors (if any):\n");
	      while ( (s = stdError.readLine ()) != null) {
	    	  System.out.println(s);
	    	   bw.write(s+"\n");
	      }
	      
	    } catch (IOException e) {

	    	e.printStackTrace();
		
	      } finally {

	    	  try {

	    		  if (bw != null)
	    			  bw.close();

	    		  if (fw != null)
	    			  fw.close();

	    	  } catch (IOException ex) {

	    		  ex.printStackTrace();

	    	  }

	      }

	  }
}
