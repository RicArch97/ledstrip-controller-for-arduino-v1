package Ledstripcontroller;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JProgressBar;

public class ReadFile {
	
public void readFile(String FILENAME, JProgressBar progressBarStatic, JProgressBar progressBarDynamic) {
		
		BufferedReader br = null;
		FileReader fr = null; 
		try {
			
			fr = new FileReader(FILENAME);
			br = new BufferedReader(fr);
			
		    String line = null;

		    while ((line = br.readLine()) != null) {
		        
		        if (line.contains("can't open device")) {
		        	progressBarStatic.setString("Upload Failed, COM port not found");
		        	progressBarDynamic.setString("Upload Failed, COM port not found");
		        }
		        
		        else if (line.contains("verifying ...")) {
		        	progressBarStatic.setString("Upload Complete!");
		        	progressBarStatic.setValue(100);
		        	
		        	progressBarDynamic.setString("Upload Complete");
		        	progressBarDynamic.setValue(100);
		        }
		    }
		    
		} catch (IOException e) {
			
			e.printStackTrace();
			
		} finally {
		    try {
				br.close();
			} catch (IOException e) {
		
				e.printStackTrace();
			}
		}
	}
}
