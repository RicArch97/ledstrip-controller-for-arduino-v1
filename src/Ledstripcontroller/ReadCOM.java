package Ledstripcontroller;

import java.util.ArrayList;
import com.fazecast.jSerialComm.*;

public class ReadCOM {
	int initialLength = 0;
	ArrayList<Devices> dev;
	int portCounter = 1;
	Elements elements;
	
	public void readCOM(Elements elements, ArrayList<Devices> dev)
	{
		this.elements = elements;
		this.dev = dev;
		
		initialLength = SerialPort.getCommPorts().length;
		SerialPort serialPorts[] = new SerialPort[initialLength];
		serialPorts = SerialPort.getCommPorts();
		
		for (int i = 0; i < initialLength; i++) {
			String portName = serialPorts[i].getDescriptivePortName();
			String comName = serialPorts[i].getSystemPortName();

			if (portName.contains("Arduino Mega 2560") || portName.contains("Arduino Uno") || portName.contains("USB-SERIAL CH340")) {
				dev.add(new Devices(portName, comName, portCounter));
				
				elements.getCOMDynamic().addItem(portName);
				elements.getCOMStatic().addItem(portName);
				portCounter++;
			}
		}
	}
	
	public void checkDevices() 
	{
		ArrayList<String> COMports = new ArrayList<String>();
		
		int len = SerialPort.getCommPorts().length;
		
		for (int j = 0; j < dev.size(); j++) {
			COMports.add(dev.get(j).getComName());
		}
		
		if (initialLength < len) {

			SerialPort serialPorts[] = new SerialPort[len];
			serialPorts = SerialPort.getCommPorts();

			for (int i = 0; i < len; i++) {
				String portName = serialPorts[i].getDescriptivePortName();
				String comName = serialPorts[i].getSystemPortName();

				if (COMports.contains(comName)) {}

				else {
					if (portName.contains("Arduino Mega 2560") || portName.contains("Arduino Uno") || portName.contains("USB-SERIAL CH340")) {
						dev.add(new Devices(portName, comName, portCounter));
						
						elements.getCOMDynamic().addItem(portName);
						elements.getCOMStatic().addItem(portName);
						portCounter++;
					}
				}
			}
			initialLength = len;
		}
		
		else if (initialLength > len) {
			ArrayList<String> COMportsDisconn = new ArrayList<String>();
			SerialPort serialPorts[] = new SerialPort[len];
			serialPorts = SerialPort.getCommPorts();

			for (int i = 0; i < len; i++) {
				COMportsDisconn.add(serialPorts[i].getSystemPortName());
			}

			COMports.removeAll(COMportsDisconn);

			if (COMports.isEmpty() == false) {
				for (int j = 0; j < dev.size(); j++) {
					if (dev.get(j).getComName().contains(COMports.get(0))) {
						elements.getCOMDynamic().removeItemAt(dev.get(j).getIndex());
						elements.getCOMStatic().removeItemAt(dev.get(j).getIndex());
						dev.remove(dev.get(j));
						portCounter--;
					}
				}
			}
			// [1 - 2 - 3 - 4] -> [1 - 2 - 4]
			for (int k = 0; k < dev.size() - 1; k++) {
				if ((dev.get(k + 1).getIndex()) - (dev.get(k).getIndex()) == 2) {
					dev.get(k + 1).setIndex(dev.get(k + 1).getIndex() - 1);
				}
			}
			
			initialLength = len;
		}
		
		else if (initialLength == len) {}
	}
}
