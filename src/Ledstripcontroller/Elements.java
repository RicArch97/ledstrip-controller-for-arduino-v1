package Ledstripcontroller;

import javax.swing.JComboBox;
import javax.swing.JProgressBar;

public class Elements {
	private JProgressBar progressBarDynamic, progressBarStatic;
	private JComboBox<String> COMPortsDynamic, ArduinoDynamic, COMPortsStatic, ArduinoStatic;
	
	public Elements() {
		progressBarDynamic = new JProgressBar(0, 100);
		progressBarStatic = new JProgressBar(0, 100);
		COMPortsDynamic = new JComboBox<String>();
		COMPortsStatic = new JComboBox<String>();
		ArduinoStatic = new JComboBox<String>();
		ArduinoDynamic = new JComboBox<String>();
	}
	
	public JProgressBar getDynamic() {
		return progressBarDynamic;
	}
	
	public JProgressBar getStatic() {
		return progressBarStatic;
	}
	
	public JComboBox<String> getCOMDynamic() {
		return COMPortsDynamic;
	}
	
	public JComboBox<String> getCOMStatic() {
		return COMPortsStatic;
	}
	
	public JComboBox<String> getArduinoDynamic() {
		return ArduinoDynamic;
	}
	
	public JComboBox<String> getArduinoStatic() {
		return ArduinoStatic;
	}
}
