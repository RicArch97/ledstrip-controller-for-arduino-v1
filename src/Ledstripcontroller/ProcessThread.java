package Ledstripcontroller;

public class ProcessThread extends Thread {
	private ReadCOM com;

	public ProcessThread(ReadCOM com) {
		this.com = com;
	}

	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			com.checkDevices();
		}
	}
}
