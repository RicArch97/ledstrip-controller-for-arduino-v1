package Ledstripcontroller;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class HomeTab {
	MyPanel Home;
	BufferedImage logo;
	JTextArea title1, title2;
    JTextArea homeText;
    JEditorPane webLink;
    JTabbedPane tabbedPane;
    BufferedImage img;
	
    public HomeTab(JTabbedPane tabbedPane, BufferedImage img) {
    	this.tabbedPane = tabbedPane;
    	this.img = img;
    }
    
	public void homeTab() 
	{
		Home = new MyPanel();
		Home.setImage(img);
		tabbedPane.addTab("Home", null, Home, null);
		Home.setLayout(null);
		JLabel lab = new JLabel();
		lab.setText("Home");
		lab.setForeground(Color.WHITE);
		lab.setFont(new Font("Poor Richard", Font.BOLD, 50));
		lab.setPreferredSize(new Dimension(297, 100));
		tabbedPane.setTabComponentAt(0, lab);

		try {
			logo = ImageIO.read(new File("img/Logo.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}

		JLabel logoLabel = new JLabel(new ImageIcon(logo));
		logoLabel.setBounds(540, 50, 200, 200);
		Home.add(logoLabel);

		title1 = new JTextArea();
		title1.setOpaque(false);
		title1.setEditable(false);
		title1.setBounds(100, 10, 500, 250);
		title1.setFont(new Font("Poor Richard", Font.BOLD, 100));
		title1.setText("Daily \n       Mods");
		title1.setForeground(Color.GREEN);
		Home.add(title1);

		title2 = new JTextArea();
		title2.setOpaque(false);
		title2.setEditable(false);
		title2.setBounds(875, 75, 500, 250);
		title2.setFont(new Font("Poor Richard", Font.BOLD, 50));
		title2.setText("Technology \nand more!");
		title2.setForeground(Color.RED);
		Home.add(title2);

		homeText = new JTextArea();
		homeText.setOpaque(false);
		homeText.setEditable(false);
		homeText.setBounds(100, 300, 1060, 180);
		homeText.setFont(new Font("Poor Richard", Font.BOLD, 20));
		homeText.setForeground(Color.WHITE);
		homeText.setText(
				"Welcome to our personalisation software! \nThis software controls the effects on your lestrip. On top you can see 4 tabs. First the Home tab which is this one. \nThen the Static tab, on this tab you can choose static non-moving colors. On the Dynamic tab you can choose a variety of \nmoving effects, some can be considered season or event based (like christmas or autumn). Last tab is Games, \non this tab you can run an effect once you start a certain game that fits with that game. \n\nWe hope that you enjoy our services!");
		Home.add(homeText);

		webLink = new JEditorPane();
		webLink.setEditable(false);
		webLink.setOpaque(false);
		webLink.setBounds(100, 500, 1060, 30);
		webLink.setEditorKit(JEditorPane.createEditorKitForContentType("text/html"));
		webLink.setText(
				"<B><font face=\"Poor Richard\" size=\"6\" color=\"white\">For more information refer to our website: <a href=\"https://dailymods.jimdo.com/\">Daily Mod's Webpage</a></font></B>");
		Home.add(webLink);

		webLink.addHyperlinkListener(new HyperlinkListener() {
			public void hyperlinkUpdate(HyperlinkEvent e) {
				if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
					if (Desktop.isDesktopSupported()) {
						try {
							Desktop.getDesktop().browse(e.getURL().toURI());
						} catch (IOException e1) {
							e1.printStackTrace();
						} catch (URISyntaxException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		});
	}
}
