package Ledstripcontroller;

public class Devices {
	private String portName = "";
	private String comName = "";
	private int index = 0;
	
	public Devices(String port, String com, int in) {
		this.portName = port;
		this.comName = com;
		this.index = in;
	}
	
	public String getComName() {
		return comName;
	}
	
	public String getDevicename() {
		return portName;
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int in) {
		this.index = in;
	}
}
