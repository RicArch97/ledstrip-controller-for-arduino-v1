package Ledstripcontroller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class StaticTab {
	MyPanel Static;
	JTextArea staticText;
	ArrayList<Devices> dev;
	ReadCOM com;
	Elements elements;
	JTabbedPane tabbedPane;
	BufferedImage img;
	DeviceConfig config;

	public StaticTab(JTabbedPane tabbedPane, BufferedImage img, DeviceConfig config, Elements elements, ReadCOM com, ArrayList<Devices> dev) 
	{
		this.dev = dev;
		this.com = com;
		this.elements = elements;
		this.tabbedPane = tabbedPane;
		this.img = img;
		this.config = config;
	}
	
	public void staticTab() 
	{
		Static = new MyPanel();
		Static.setImage(img);
		staticText = new JTextArea();
		tabbedPane.addTab("Static", null, Static, null);
		Static.setLayout(null);
		JLabel lab = new JLabel();
		lab.setText("Static");
		lab.setForeground(Color.WHITE);
		lab.setFont(new Font("Poor Richard", Font.BOLD, 50));
		lab.setPreferredSize(new Dimension(297, 100));
		tabbedPane.setTabComponentAt(1, lab);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(150, 50, 250, 400);
		Static.add(scrollPane);

		JList<String> list_1 = new JList<String>();
		list_1.setFont(new Font("Poor Richard", Font.BOLD, 30));

		DefaultListModel<String> dlm = new DefaultListModel<String>();
		dlm.addElement("Fire_Red");
		dlm.addElement("Red");
		dlm.addElement("Hot_Pink");
		dlm.addElement("Purple");
		dlm.addElement("Razer_Green");
		dlm.addElement("Green");
		dlm.addElement("Blue_Pond");
		dlm.addElement("Blue");
		dlm.addElement("Snow_White");
		dlm.addElement("Angel_White");
		dlm.addElement("Mint");
		dlm.addElement("Yellow");
		dlm.addElement("Black (OFF)");
		list_1.setModel(dlm);
		scrollPane.setViewportView(list_1);

		list_1.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent arg0) {

				JList<?> list = (JList<?>) arg0.getSource();
				if (!arg0.getValueIsAdjusting()) {
					elements.getStatic().setValue(0);
					elements.getStatic().setString(null);

					if (list.getSelectedValue().equals("Fire_Red")) {
						config.setHexPath("Hexfiles/Fire_red.ino.hex");
						staticText.setText("Fire Red");
						staticText.setForeground(Color.ORANGE);
					}

					if (list.getSelectedValue().equals("Mint")) {
						config.setHexPath("Hexfiles/Mint.ino.hex");
						staticText.setText("Mint");
						staticText.setForeground(Color.CYAN);
					}

					if (list.getSelectedValue().equals("Razer_Green")) {
						config.setHexPath("Hexfiles/Razer_Green.ino.hex");
						staticText.setText("Razer Green");
						staticText.setForeground(Color.GREEN);
					}

					if (list.getSelectedValue().equals("Snow_White")) {
						config.setHexPath("Hexfiles/Snow_White.ino.hex");
						staticText.setText("Snow White");
						staticText.setForeground(Color.WHITE);
					}

					if (list.getSelectedValue().equals("Blue")) {
						config.setHexPath("Hexfiles/Blue.ino.hex");
						staticText.setText("Blue");
						staticText.setForeground(Color.BLUE);
					}

					if (list.getSelectedValue().equals("Blue_Pond")) {
						config.setHexPath("Hexfiles/Blue_Pond.ino.hex");
						staticText.setText("Blue Pond");
						staticText.setForeground(Color.CYAN);
					}

					if (list.getSelectedValue().equals("Red")) {
						config.setHexPath("Hexfiles/Red.ino.hex");
						staticText.setText("Red");
						staticText.setForeground(Color.RED);
					}

					if (list.getSelectedValue().equals("Green")) {
						config.setHexPath("Hexfiles/Green.ino.hex");
						staticText.setText("Green");
						staticText.setForeground(Color.GREEN);
					}

					if (list.getSelectedValue().equals("Yellow")) {
						config.setHexPath("Hexfiles/Yellow.ino.hex");
						staticText.setText("Yellow");
						staticText.setForeground(Color.YELLOW);
					}

					if (list.getSelectedValue().equals("Angel_White")) {
						config.setHexPath("Hexfiles/Angel_White.ino.hex");
						staticText.setText("Angel White");
						staticText.setForeground(Color.WHITE);
					}

					if (list.getSelectedValue().equals("Black (OFF)")) {
						config.setHexPath("Hexfiles/Black_Off.ino.hex");
						staticText.setText("Black, ledstrip OFF");
						staticText.setForeground(Color.BLACK);
					}

					if (list.getSelectedValue().equals("Hot_Pink")) {
						config.setHexPath("Hexfiles/Hot_Pink.ino.hex");
						staticText.setText("Hot Pink");
						staticText.setForeground(Color.PINK);
					}

					if (list.getSelectedValue().equals("Purple")) {
						config.setHexPath("Hexfiles/Purple.ino.hex");
						staticText.setText("Purple");
						staticText.setForeground(Color.MAGENTA);
					}
				}
			}
		});

		JLabel lblStaticEffects = new JLabel("Static Effects");
		lblStaticEffects.setForeground(SystemColor.textHighlight);
		lblStaticEffects.setBackground(SystemColor.menu);
		lblStaticEffects.setFont(new Font("Poor Richard", Font.BOLD, 30));
		scrollPane.setColumnHeaderView(lblStaticEffects);

		JButton btnNewButton = new JButton("UPLOAD");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					RunCommand avr = new RunCommand();
					avr.runCommand(
							new String[] { "avrdude/bin/avrdude.exe", "-C" + "avrdude/etc/avrdude.conf", "-v", config.getDevice(),
									config.getDeviceVar(), "-P" + config.getPort(), config.getBaudRate(), "-D", "-Uflash:w:" + config.getHexPath() + ":i" },
							config.getFilename());

					ReadFile file = new ReadFile();
					file.readFile(config.getFilename(), elements.getStatic(), elements.getDynamic());
				} catch (Exception er) {
					er.printStackTrace();
				}
			}
		});

		btnNewButton.setFont(new Font("Poor Richard", Font.BOLD, 20));
		btnNewButton.setBounds(534, 381, 196, 69);
		Static.add(btnNewButton);

		elements.getStatic().setValue(0);
		elements.getStatic().setStringPainted(true);
		elements.getStatic().setBounds(412, 503, 718, 35);
		Static.add(elements.getStatic());

		JLabel lblUploadProgress = new JLabel("Upload progress:");
		lblUploadProgress.setForeground(Color.GREEN);
		lblUploadProgress.setFont(new Font("Poor Richard", Font.BOLD, 40));
		lblUploadProgress.setBounds(70, 486, 307, 52);
		Static.add(lblUploadProgress);

		JLabel lblNewLabel_2 = new JLabel("Choose COM-port:");
		lblNewLabel_2.setFont(new Font("Poor Richard", Font.BOLD, 20));
		lblNewLabel_2.setForeground(Color.GREEN);
		lblNewLabel_2.setBounds(874, 361, 256, 35);
		Static.add(lblNewLabel_2);

		elements.getArduinoStatic().setBounds(874, 300, 256, 43);
		elements.getArduinoStatic().addItem("Choose model:");
		elements.getArduinoStatic().addItem("Arduino Nano");
		elements.getArduinoStatic().addItem("Arduino Uno");
		elements.getArduinoStatic().addItem("Arduino Mega 2560");
		Static.add(elements.getArduinoStatic());

		elements.getArduinoStatic().addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent arg0) {

				Object item = elements.getArduinoStatic().getSelectedItem();
				
				if (item == "Arduino Nano") {
					config.setDevice("-patmega328p");
					config.setDeviceVar("-carduino");
					config.setBaudRate("-b57600");
				}

				if (item == "Arduino Uno") {
					config.setDevice("-patmega328p");
					config.setDeviceVar("-carduino");
					config.setBaudRate("-b115200");
				}

				if (item == "Arduino Mega 2560") {
					config.setDevice("-patmega2560");
					config.setDeviceVar("-cwiring");
					config.setBaudRate("-b115200");
				}

				if (elements.getArduinoDynamic().getSelectedItem() != elements.getArduinoStatic().getSelectedItem()) {
					elements.getArduinoDynamic().setSelectedItem(elements.getArduinoStatic().getSelectedItem());
				}
			}
		});

		JLabel lblNewLabel_3 = new JLabel("Choose Arduino model:");
		lblNewLabel_3.setFont(new Font("Poor Richard", Font.BOLD, 20));
		lblNewLabel_3.setForeground(Color.GREEN);
		lblNewLabel_3.setBounds(874, 251, 256, 35);
		Static.add(lblNewLabel_3);

		staticText.setOpaque(false);
		staticText.setEditable(false);
		staticText.setFont(new Font("Poor Richard", Font.BOLD, 50));
		staticText.setBounds(630, 50, 500, 175);
		Static.add(staticText);

		elements.getCOMStatic().setBounds(874, 407, 256, 43);
		Static.add(elements.getCOMStatic());

		elements.getCOMStatic().addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent arg0) {

				String SerialPort = elements.getCOMStatic().getSelectedItem().toString();

				for (int i = 0; i < dev.size(); i++) {
            		if (SerialPort.contains(dev.get(i).getComName())) {
            			config.setPort(dev.get(i).getComName());
            		}
            	}

				if (!(elements.getCOMDynamic().getSelectedItem().equals(elements.getCOMStatic().getSelectedItem()))) {
					elements.getCOMDynamic().setSelectedItem(SerialPort);
				}
			}
		});
	}
}
