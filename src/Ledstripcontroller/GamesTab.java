package Ledstripcontroller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.image.BufferedImage;

import javax.swing.JLabel;
import javax.swing.JTabbedPane;

public class GamesTab {
	MyPanel Games;
	JTabbedPane tabbedPane;
	BufferedImage img;
	
	public GamesTab(JTabbedPane tabbedPane, BufferedImage img) {
		this.tabbedPane = tabbedPane;
		this.img = img;
	}
	
	public void gamesTab() 
	{
		Games = new MyPanel();
		Games.setImage(img);
		tabbedPane.addTab("Games", null, Games, null);
		Games.setLayout(null);
		JLabel labhome = new JLabel();
		labhome.setText("Games");
		labhome.setForeground(Color.WHITE);
		labhome.setFont(new Font("Poor Richard", Font.BOLD, 50));
		labhome.setPreferredSize(new Dimension(297, 100));
		tabbedPane.setTabComponentAt(3, labhome);
	}
}
