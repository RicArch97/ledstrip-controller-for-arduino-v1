package Ledstripcontroller;
// Copyright: Ricardo Steijn, Alec van der Linden
// Students of Rotterdam University and Avans University Breda
// This program lets the user choose an effect for a 60 led ledstrip, and upload it to Arduino.

import java.awt.EventQueue;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JTabbedPane;

public class MainGUI {
	JFrame frame;
	BufferedImage img;

	public static void main(String[] args) throws IOException {
		UIManager.put("TabbedPane.selected", Color.DARK_GRAY);
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI window = new MainGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainGUI() throws IOException {
		initialize();
	}

	private void initialize() throws IOException 
	{
		frame = new JFrame();  
		frame.setBounds(600, 150, 1280, 720);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 1280, 691);
		tabbedPane.setBackground(Color.BLACK);
		frame.getContentPane().add(tabbedPane);
		
		try {
            img = ImageIO.read(new File("img/Background.png"));
        } catch(IOException e) {
            e.printStackTrace();
        }
		
		ArrayList<Devices> dev = new ArrayList<Devices>();
		Elements elements = new Elements();
		elements.getCOMStatic().addItem("Choose port:");
		elements.getCOMStatic().setSelectedItem("Choose port:");
		elements.getCOMDynamic().addItem("Choose port:");
		elements.getCOMDynamic().setSelectedItem("Choose port:");
		
		ReadCOM com = new ReadCOM();
		com.readCOM(elements, dev);
		ProcessThread thread = new ProcessThread(com);
		thread.start();
		
		DeviceConfig config = new DeviceConfig();
		HomeTab home = new HomeTab(tabbedPane, img);
		StaticTab staticEffect = new StaticTab(tabbedPane, img, config, elements, com, dev);
		DynamicTab dynamicEffect = new DynamicTab(tabbedPane, img, config, elements, com, dev);
		GamesTab games = new GamesTab(tabbedPane, img);
		
		home.homeTab();
		staticEffect.staticTab();
		dynamicEffect.dynamicTab();
		games.gamesTab();
	}
}
